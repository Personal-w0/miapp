import { HomeComponent } from './components/home/home.component';
import { ConversorComponent } from './components/conversor/conversor.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { TipoCambioComponent } from "./components/tipo-cambio/tipo-cambio.component";

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "tipocambio", component: TipoCambioComponent },
  { path: "conversor", component: ConversorComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
