export interface TipoCambio {
  moneda_origen: number;
  moneda_destino: number;
  tipo_cambio: number;
}
