export interface Historial {
  Id: number;
  monto: number;
  monto_con_tipo_cambio: number;
  moneda_origen: number;
  moneda_destino: number;
  tipo_cambio: number;
}
