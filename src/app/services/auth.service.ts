import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from "firebase/app";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  provide: any;
  constructor(private afAuth: AngularFireAuth) {}

  logaut() {
    return this.afAuth.signOut();
  }

  // POR VALIDAR
  changeSession() {
    return this.afAuth.onAuthStateChanged(function (user) {
      console.log("Por valir ChangeSession");
      if (user) {
        console.log("User: ", user);
      } else {
        console.log("No User: ", user);
      }
    });
  }

  registerUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.createUserWithEmailAndPassword(email, pass).then(
        (userData) => resolve(userData),
        (err) => reject(err)
      );
    });
  }

  loginEmail(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.signInWithEmailAndPassword(email, pass).then(
        (userData) => resolve(userData),
        (err) => reject(err)
      );
    });
  }

  loginGoogle() {
    return this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  loginFacebook() {
    // return this.afAuth.auth.signInWithPopup(
    //   new firebase.auth.FacebookAuthProvider()
    // );

    return this.afAuth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(function (result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        // var token = result.credential.accessToken;
        // The signed-in user info.
        // var user = result.user;
      })
      .catch(function (error) {
        if (error.code === "auth/account-exists-with-different-credential") {
          firebase
            .auth()
            .fetchSignInMethodsForEmail(error.email)
            .then(function (methods) {
              if (methods[0] === "password") {
                firebase
                  .auth()
                  .signInWithEmailAndPassword(error.email, "")
                  .then(function (user) {})
                  .then(function () {
                    // Facebook account successfully linked to the existing Firebase user.
                    // goToApp();
                  });
                return;
              }
              firebase
                .auth()
                .signInWithPopup(new firebase.auth.GoogleAuthProvider())
                .then(function (result) {
                  result.user
                    .linkAndRetrieveDataWithCredential(error.credential)
                    .then(function (usercred) {
                      // Facebook account successfully linked to the existing Firebase user.
                    });
                });
            });
        }
      });
  }

  getAuth() {
    return this.afAuth.authState.pipe((user) => user);
  }
}
