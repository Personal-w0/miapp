import { AuthService } from "./auth.service";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { TipoCambio } from "../models/tipo-cambio-model";
import { Historial } from "../models/historial.model";

@Injectable({
  providedIn: "root",
})
export class TipoCambioService {
  base = "https://webapixxyy.azurewebsites.net/api/";
  //base = "https://localhost:5001/api/";
  headers = new HttpHeaders();
  headers2 = new HttpHeaders();
  params = new HttpParams();

  constructor(protected http: HttpClient, private authService: AuthService) {
    /*
    if(this.isLogin()){
      console.log('Ya existe session');        
    }else{      
      console.log('Iniciando session:');
      this.login('willy0liver','123123');    
    }
    */

    this.login("willy0liver", "123123");
  }

  login(usu: string, pass: string) {
    this.http
      .post(this.base + "/login", { usuario: usu, password: pass })
      .subscribe((resp: any) => {
        //console.log(resp);
        localStorage.setItem("auth_token", resp.token);
      });
    this.headers = new HttpHeaders({
      Authorization: `Bearer ${localStorage.getItem("auth_token")}`,
      Accept: "application/json",
    });
  }

  listarTipoCambio() {
    return this.http.get<TipoCambio[]>(this.base + "tipocambio", {
      headers: this.headers,
    });
  }

  listarHistorial() {
    return this.http.get<Historial[]>(this.base + "consulta", {
      headers: this.headers,
    });
  }

  actualizarTipoCambio(tc: any) {
    return this.http.post(this.base + "tipocambio/actualizar", tc, {
      headers: this.headers,
      responseType: "text",
    });
  }

  cambiar(entrada: any) {
    return this.http.post(
      this.base + "consulta/cambiar",
      {
        monto: parseFloat (entrada.monto),
        moneda_origen: parseInt(entrada.moneda_origen),
        moneda_destino: parseInt(entrada.moneda_destino)
      },
      {
        headers: this.headers,
        responseType: "json",
      }
    );
  }

  logout() {
    localStorage.removeItem("auth_token");
  }

  isLogin(): boolean {
    return localStorage.getItem("auth_token") !== null;
  }
}
