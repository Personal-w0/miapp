import { TipoCambioService } from "./../../services/tipo-cambio.service";
import { Historial } from "./../../models/historial.model";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-conversor",
  templateUrl: "./conversor.component.html",
  styleUrls: ["./conversor.component.scss"],
})
export class ConversorComponent implements OnInit {
  tcList: Array<Historial> = [];
  oHistorial: any;
  origen: number;
  destino: number;
  monto: number;

  etiqueta_origen: "";
  etiqueta_destino: "";
  monto_calculado: any;

  optionsSelect: Array<any>;

  constructor(private servicio: TipoCambioService) {
    this.tcList = new Array<any>();
  }

  onCambiar(): void {
    console.log("Paso 01");

    this.oHistorial = {
      monto: this.monto,
      moneda_origen: this.origen,
      moneda_destino: this.destino,
    };
    var aaa = this.servicio.cambiar(this.oHistorial);
    aaa.subscribe(
      (a: Historial) => {
        this.monto_calculado =
          a.monto_con_tipo_cambio + " " + this.etiqueta_destino;
      },
      (err) => console.log("Errorrr:", err)
    );
  }

  ngOnInit(): void {
    this.optionsSelect = [
      { value: "1", label: "Soles" },
      { value: "2", label: "Dólares" },
      { value: "3", label: "Euros" },
    ];
  }

  optionSelectedOrigen(ev) {
    this.monto_calculado = null;
    this.etiqueta_origen = ev.label;
  }

  optionSelectedDestino(ev) {
    this.monto_calculado = null;
    this.etiqueta_destino = ev.label;
  }
}
