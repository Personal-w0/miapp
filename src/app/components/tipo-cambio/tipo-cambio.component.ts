import { TipoCambio } from '../../models/tipo-cambio-model';
import Swal from 'sweetalert2'
import { TipoCambioService } from "./../../services/tipo-cambio.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-tipo-cambio",
  templateUrl: "./tipo-cambio.component.html",
  styleUrls: ["./tipo-cambio.component.scss"],
})
export class TipoCambioComponent implements OnInit {

  tcList: Array<TipoCambio> = [];
  otc:any;

  constructor(private servicio: TipoCambioService) {
    this.tcList = new Array<any>();
    servicio.listarTipoCambio().subscribe((a) => {
      this.tcList = a;
    });
  }

  ngOnInit(): void {
    console.log("this.tcList", this.tcList);
  }

  changeValue(origen: number, destino: number, event: any) {
    console.log("Evento:", event.target.textContent);

    //this.editField = event.target.textContent;

    this.otc = { moneda_origen: origen, moneda_destino: destino, tipo_cambio: parseFloat(event.target.textContent) };

    this.servicio.actualizarTipoCambio(this.otc).subscribe(
      (a) => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: a,
          showConfirmButton: false,
          timer: 3500
        })
      },
      (err) => {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: err,
          showConfirmButton: false,
          timer: 3500
        })
      }
    );
  }

  updateList(origen: number, destino: number, event: any) {
    const editField = event.target.textContent;
  }

  remove(id: any) {
    this.por_implementar();
    
  }

  por_implementar(){
    Swal.fire({
      position: 'top-end',
      icon: 'info',
      title: 'Por implementar',
      showConfirmButton: false,
      timer: 1500
    })
  }

  add() {
    this.por_implementar();
  }
}
