import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public nameUser: string;
  public emailUser: string;
  public isLogin: boolean;
  public fotoUser: string;
  public visible = false;

  constructor(public authService: AuthService, public router: Router) {
    console.log("HeaderComponent Ini 1");
  }

  ngOnInit() {
    this.authService.getAuth().subscribe(auth => {
      console.log("HeaderComponent Ini 2");
      if (auth) {
        this.nameUser = auth.displayName;
        this.emailUser = auth.email;
        this.isLogin = true;
        this.fotoUser = auth.photoURL;
      } else {
        this.isLogin = false;
      }
      console.log("Logueado: " + this.isLogin);
      console.log(auth);
    });
  }
  onClickLogaut() {
    this.authService
      .logaut()
      .then(function() {
        console.log("Deslogueado OK");
      })
      .catch(function() {
        console.log("Esta Logueado OK");
      });
  }

}
