import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from "@angular/core";
import Swal from 'sweetalert2'
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public email: string;
  public password: string;
  constructor(public authService: AuthService, public router: Router) { }
  ngOnInit(): void { }
  onSubmitLogin() {
    this.authService
      .loginEmail(this.email, this.password)
      .then(res => {
        
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Logueado correctamente',
          showConfirmButton: false,
          timer: 1500
        })

        this.router.navigate(["/tipocambio"]);
      })
      .catch(err => {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: err.message,
          showConfirmButton: true
          //timer: 1500
        })

        this.router.navigate(["/login"]);
      });
  }
  onSubmitGoogleLogin() {
    console.log("Iniciooo");
    this.authService
      .loginGoogle()
      .then(res => {
        console.log("Biennnnn");
        this.router.navigate(["/tipocambio"]);
      })
      .catch(err => {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: err.message,
          showConfirmButton: false,
          timer: 1500
        })
        this.router.navigate(["/login"]);
      });
  }
  onSubmitFacebookLogin() {
    console.log("Iniciooo");
    this.authService
      .loginFacebook()
      .then(res => {
        console.log("Biennnnn");
        this.router.navigate(["/reserva"]);
      })
      .catch(err => {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: err.message,
          showConfirmButton: false,
          timer: 1500
        })
        this.router.navigate(["/login"]);
      });
  }

}
