import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public email: string;
  public password: string;

  constructor(
    public authService: AuthService,
    public router: Router
  ) {}

  ngOnInit() {}

  public onSubmitAddUser() {
    this.authService
      .registerUser(this.email, this.password)
      .then(res => {
        console.log('Registro exitoso');
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Registro Existoso',
          showConfirmButton: false,
          timer: 2500
        })
        this.router.navigate(['/tipocambio']);
      })
      .catch(err => {
        
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: err.message,
          showConfirmButton: false,
          timer: 2500
        })
      });
  }

}
