
import { RouterModule } from '@angular/router';


import { TipoCambioService } from './services/tipo-cambio.service';
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { AgmCoreModule } from "@agm/core";
import { AppComponent } from "./app.component";

// Firevase
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from './../environments/environment';


import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import {
  MDBSpinningPreloader,
  MDBBootstrapModulesPro,
  ToastModule,
} from "ng-uikit-pro-standard";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { AppRoutingModule } from "./app-routing.module";
import { TipoCambioComponent } from './components/tipo-cambio/tipo-cambio.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ConversorComponent } from './components/conversor/conversor.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [AppComponent, NavbarComponent, TipoCambioComponent, LoginComponent, RegisterComponent, ConversorComponent, HomeComponent, FooterComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: "Mduj58nsFTzrF_M9jz3d",
    }),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    SweetAlert2Module,
    RouterModule
  ],
  providers: [MDBSpinningPreloader, TipoCambioService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
