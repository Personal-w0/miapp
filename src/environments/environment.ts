// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDXDhzOPOrIr8QXUD4e0yY61QwGTyK65-w",
    authDomain: "exambcp-ca694.firebaseapp.com",
    databaseURL: "https://exambcp-ca694.firebaseio.com",
    projectId: "exambcp-ca694",
    storageBucket: "exambcp-ca694.appspot.com",
    messagingSenderId: "1084613837915",
    appId: "1:1084613837915:web:742deddd1d2e58b20a6b7e",
    measurementId: "G-QSX9FKDW6C",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
